#!/usr/bin/env python3


"""
This file is part of oneov-merge.

oneov-merge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

oneov-merge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with oneov-merge.  If not, see <https://www.gnu.org/licenses/>.

Original author: Maciej Barć <xgqt@riseup.net>
Copyright (c) 2021, src_prepare group
Licensed under the GNU GPL v2 License


Objective:
 - parse repos list and get all git repos
 - clone repos into coreect directories in tmp, --depth 1
 - copy files starting with oldest to youngest repo (get HEAD commit)
 - run repoman ?

TODO:
 - git timeout
"""


import xml.etree.ElementTree as ET

from git import Repo
from os import listdir, mkdir
from os.path import exists, isdir, isfile, join
from requests import get as GET

from private.cp import cp
from private.layout import write_layout


temp_dir = "/tmp/oneov-merge/"

file_dir = join(temp_dir, "files")
work_dir = join(temp_dir, "work")

ovls_dir = join(work_dir, "overlays")
oneov_dir = join(work_dir, "oneov")
oneov_profiles_dir = join(oneov_dir, "profiles")


# This is a list of user-submitted overlays
repositories_xml_url = "https://raw.githubusercontent.com/\
gentoo/api-gentoo-org/master/files/overlays/repositories.xml"

repositories_xml_file = join(file_dir, "repositories.xml")


# Exclude those overlays
banned_overlays = ["gentoo"]


def get_overlays() -> dict:
    """Generate a overlays dict."""

    overlays_dict = {}

    with open(repositories_xml_file) as f:

        repos = ET.parse(f).getroot().findall("./repo")
        for repo in repos:
            name = ""

            # search for overlay name
            for prop in repo:
                if prop.tag == "name":

                    _name = prop.text
                    if _name not in banned_overlays:  # excludes
                        name = _name

                    if name != "":
                        overlays_dict[name] = ""

            # search for overlay source url (upstream we can pull with git)
            for prop in repo:
                if name != "" and prop.tag == "source":
                    # exclude URLs with SSH (ie.: git+ssh)
                    # because they will probably need authentication
                    if prop.attrib["type"] == "git" and "ssh" not in prop.text:
                        overlays_dict[name] = prop.text

    return overlays_dict


def clone_overlays(overlays: dict) -> None:
    """Parse OVERLAYS dict and clone the overlays."""

    num_overlays = 0

    for name, url in overlays.items():

        if url == "":
            print("Overlay %s has no supported URL, sorry" % name)
            continue

        clone_dir = join(ovls_dir, name)
        if exists(clone_dir):
            print("Overlay already exists: %s" % name)
            num_overlays += 1
        else:
            print("Cloning overlay: %s" % name)
            print("  From URL: %s" % url)

            try:
                Repo.clone_from(url, clone_dir, depth=1)
                num_overlays += 1
            except Exception:
                print("Failed to clone repository %s" % name)

    print("Cloned %d overlays!" % num_overlays)


def sort_overlays(overlays: dict) -> list:
    """Sort overlays list based on oldest (to latest) commit."""

    # listof ( list ( overlay & timestamp ) )
    overlays_timestamps = []

    for overlay in overlays:

        repo_dir = join(ovls_dir, overlay)
        if isdir(repo_dir):

            commit_date = Repo(repo_dir).head.commit.committed_date
            # younger than 2019.12.30
            if commit_date > 1577667600:

                overlays_timestamps.append([overlay, commit_date])

    # oldest will be first
    overlays_timestamps.sort(key=(lambda lst: lst[1]))
    return [lst[0] for lst in overlays_timestamps]


def merge_overlays(overlays: list) -> None:
    """Merge overlays into one."""

    banned_dirs = [
        "3rd_party", "generator", "helpers", "script", "scripts", "tools",
        "Documentation", "doc", "docs", "scribblings",
        "archives", "tests", "updates"
        "bin", "etc", "misc", "prefix", "patches",
        "cache", "status",
        "examples", "sets",
        "kernel",
        "layman", "mask", "profiles",
        "licences"  # spelling mistake
    ]

    for overlay in overlays:
        print("Copying %s" % overlay)
        overlay_dir = join(ovls_dir, overlay)  # single overlay directory

        for subdir in listdir(overlay_dir):
            if subdir in banned_dirs or subdir[0] == "_" or "." in subdir:
                continue

            full_subdir = join(overlay_dir, subdir)
            if isdir(full_subdir):
                cp(full_subdir, join(oneov_dir, subdir))


def get_categories(overlays: dict) -> list:
    """
    Additional package categories found in overlays "profiles/categories".
    """
    categories = []

    for overlay in overlays:

        categories_file = join(ovls_dir, overlay, "profiles/categories")
        if isfile(categories_file):
            with open(categories_file) as f:
                categories += f.readlines()

    return categories


def write_profiles(categories: list) -> None:
    """Write a CATEGORIES list into oneov's "profiles/categories"."""

    with open(join(oneov_profiles_dir, "categories"), "w") as f:
        f.writelines("".join(sorted(categories)))

    open(join(oneov_profiles_dir, "repo_name"), "w").writelines("oneov")


def main():
    """Main function."""

    for d in temp_dir, file_dir, work_dir, oneov_dir, oneov_profiles_dir:
        if not isdir(d):
            mkdir(d)

    open(repositories_xml_file, "wb").write(
        GET(repositories_xml_url, allow_redirects=True).content)

    overlays = get_overlays()
    clone_overlays(overlays)

    merge_overlays(sort_overlays(overlays))
    write_profiles(get_categories(overlays))
    write_layout(oneov_dir)


if __name__ == "__main__":
    main()
