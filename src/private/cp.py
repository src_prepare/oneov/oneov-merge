#!/usr/bin/env python3

"""
This file is part of oneov-merge.

oneov-merge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

oneov-merge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with oneov-merge.  If not, see <https://www.gnu.org/licenses/>.

Original author: Maciej Barć <xgqt@riseup.net>
Copyright (c) 2021, src_prepare group
Licensed under the GNU GPL v2 License
"""


from subprocess import run


def cp(source, destination):
    """
    Copy source to destination, overwrite everything.
    """

    run(["cp", "-R", "-T", source, destination])
