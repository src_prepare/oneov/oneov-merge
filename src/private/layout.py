#!/usr/bin/env python3


"""
This file is part of oneov-merge.

oneov-merge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

oneov-merge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with oneov-merge.  If not, see <https://www.gnu.org/licenses/>.

Original author: Maciej Barć <xgqt@riseup.net>
Copyright (c) 2021, src_prepare group
Licensed under the GNU GPL v2 License
"""


from os.path import join
from pathlib import Path


layout = [
    "masters = gentoo",
    "cache-formats = md5-dict",
    "manifest-hashes = BLAKE2B SHA512",
    "manifest-required-hashes = BLAKE2B",
    "profile-formats = portage-2 profile-set",
    "sign-commits = true",
    "sign-manifests = false",
    "thin-manifests = true",
    "update-changelog = false",
    "eapis-banned = 0 1 2 3"
]


def write_layout(pth):
    """Write "metadata/layout.conf" in PTH."""

    metadata_dir = join(pth, "metadata")
    Path(metadata_dir).mkdir(parents=True, exist_ok=True)

    with open(join(metadata_dir, "layout.conf"), "w") as f:
        f.writelines("\n".join(layout))
